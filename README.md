# Recipe App API Proxy

NGINX proxy app for our recipe app API

## Usage

### Environtment Variables

 *`LISTEN_PORT` - Port to listen on (default: `8080`)
 * `APP_HOST` - Hostname of the app to forward request to (default: `app`)
 * `APP_PORT` - Port fot the app to fordward requests to (default: `9000`)
